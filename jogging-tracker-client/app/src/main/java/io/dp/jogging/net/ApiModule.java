package io.dp.jogging.net;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.squareup.otto.Bus;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.dp.jogging.BuildConfig;
import io.dp.jogging.Const;
import io.dp.jogging.event.RestApiErrorEvent;
import io.dp.jogging.event.UnauthorizedAccessEvent;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import timber.log.Timber;

/**
 * Created by dp on 07/09/14.
 */
@Module(complete = false,
        library = true)
public class ApiModule {

  @Singleton
  @Provides
  public RestApi provideRestApi(final Bus bus, final SharedPreferences prefs) {
    RestAdapter adapter = new RestAdapter.Builder()
        .setEndpoint(BuildConfig.SERVER_URL + "/api/v1/")
        .setExecutors(Executors.newCachedThreadPool(), Executors.newSingleThreadExecutor())
        .setErrorHandler(new ErrorHandler() {
          @Override
          public Throwable handleError(RetrofitError cause) {
            retrofit.client.Response r = cause.getResponse();
            if (r != null) {
              int status = r.getStatus();

              if (status == 401) {
                bus.post(new UnauthorizedAccessEvent(cause));
                return cause;
              }
            }

            bus.post(new RestApiErrorEvent(cause));
            return cause;
          }
        })
        .setRequestInterceptor(new RequestInterceptor() {
          @Override
          public void intercept(RequestFacade request) {
            String accessToken = prefs.getString(Const.ACCESS_TOKEN, null);
            if (!TextUtils.isEmpty(accessToken)) {
              Timber.v("Authorization " + accessToken);
              request.addHeader("Authorization", "Bearer " + accessToken);
            }
          }
        })
        .build();

    if (BuildConfig.DEBUG) {
      adapter.setLogLevel(RestAdapter.LogLevel.FULL);
    }

    return adapter.create(RestApi.class);
  }

  @Singleton
  @Provides
  public RegistrationApi provideRegistrationApi() {
    RestAdapter adapter = new RestAdapter.Builder()
        .setEndpoint(BuildConfig.SERVER_URL)
        .build();

    if (BuildConfig.DEBUG) {
      adapter.setLogLevel(RestAdapter.LogLevel.FULL);
    }

    return adapter.create(RegistrationApi.class);
  }
}
