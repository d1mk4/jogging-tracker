package io.dp.jogging.activity;

import android.os.Bundle;

import io.dp.jogging.R;
import io.dp.jogging.fragment.TimesFragment;

public class MainActivity extends BaseActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    getSupportFragmentManager().beginTransaction()
        .replace(R.id.container, new TimesFragment(), TimesFragment.class.getSimpleName())
        .commit();
  }
}
