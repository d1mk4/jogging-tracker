package io.dp.jogging.event;

import io.dp.jogging.db.Time;

/**
 * Created by dp on 16/09/14.
 */
public class DeleteActionEvent {

  Time time;

  public DeleteActionEvent(Time item) {
    this.time = item;
  }

  public Time getTime() {
    return time;
  }
}
