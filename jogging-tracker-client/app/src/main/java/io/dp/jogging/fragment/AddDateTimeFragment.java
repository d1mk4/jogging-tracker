package io.dp.jogging.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.sql.SQLException;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import io.dp.jogging.R;
import io.dp.jogging.annotation.FragmentLayout;
import io.dp.jogging.db.DatabaseHelper;
import io.dp.jogging.db.Time;
import rx.Observable;
import rx.Subscriber;
import rx.android.observables.AndroidObservable;

/**
 * Created by dp on 07/09/14.
 */

@FragmentLayout(R.layout.fragment_add_date_time)
public class AddDateTimeFragment extends BaseFragment {

  @Inject
  DatabaseHelper dbHelper;

  @InjectView(R.id.datePicker)
  DatePicker datePickerView;

  @InjectView(R.id.timePicker)
  TimePicker timePickerView;

  public static AddDateTimeFragment newInstance(long timeId) {
    AddDateTimeFragment f = new AddDateTimeFragment();
    Bundle args = new Bundle();
    args.putLong(Time.TIME_ID, timeId);
    f.setArguments(args);
    return f;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    getActivity().setTitle(R.string.hint_fill_in_date_time);

    Bundle args = getArguments();
    long timeId = args.getLong(Time.TIME_ID, -1);

    if (timeId > -1) {
      try {
        Time time = dbHelper.getTimesDao().queryForId(timeId);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time.getDate());
        datePickerView.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                                  calendar.get(Calendar.DAY_OF_MONTH));

        timePickerView.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        timePickerView.setCurrentMinute(calendar.get(Calendar.MINUTE));
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  @OnClick(R.id.next)
  public void onNextClick(View v) {

    int dayOfMonth = datePickerView.getDayOfMonth();
    int month = datePickerView.getMonth();
    int year = datePickerView.getYear();

    Integer hour = timePickerView.getCurrentHour();
    Integer minute = timePickerView.getCurrentMinute();

    Calendar calendar = Calendar.getInstance();
    calendar.set(year, month, dayOfMonth, hour, minute);

    long timeId = getArguments().getLong(Time.TIME_ID, -1);
    getFragmentManager().beginTransaction()
        .replace(R.id.container, AddTimeDistanceFragment.newInstance(timeId, calendar.getTimeInMillis()))
        .addToBackStack(null)
        .commit();
  }

  @OnClick(R.id.cancel)
  public void onCancelClick(View v) {
    getFragmentManager().popBackStack();
  }
}
