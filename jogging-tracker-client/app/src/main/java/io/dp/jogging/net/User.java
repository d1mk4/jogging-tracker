package io.dp.jogging.net;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by dp on 14/09/14.
 */
public class User {
  String username;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public static Observable<Token> signUp(final Credentials c, final RegistrationApi api) {
    return Observable.just(c).flatMap(new Func1<Credentials, Observable<Credentials>>() {
      @Override
      public Observable<Credentials> call(final Credentials credentials) {
        return Observable.create(new Observable.OnSubscribe<Credentials>() {
          @Override
          public void call(Subscriber<? super Credentials> subscriber) {
            try {
              credentials.setPassword(encodePassword(credentials.getPassword()));
              subscriber.onNext(credentials);
            } catch (NoSuchAlgorithmException e) {
              subscriber.onError(e);
            } finally {
              subscriber.onCompleted();
            }
          }
        });
      }
    }).flatMap(new Func1<Credentials, Observable<User>>() {
      @Override
      public Observable<User> call(Credentials credentials) {
        return api.signup(credentials.getLogin(), credentials.getPassword());
      }
    }).flatMap(new Func1<User, Observable<Token>>() {
      @Override
      public Observable<Token> call(User user) {

        return api
            .getToken(RegistrationApi.authHeader, c.getLogin(), c.getPassword(), "password", "read write", "123456", "clientapp");
      }
    }).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public static Observable<Token> login(final Credentials c, final RegistrationApi api) {
    return Observable.just(c).flatMap(new Func1<Credentials, Observable<Credentials>>() {
      @Override
      public Observable<Credentials> call(final Credentials credentials) {
        return Observable.create(new Observable.OnSubscribe<Credentials>() {
          @Override
          public void call(Subscriber<? super Credentials> subscriber) {
            try {
              credentials.setPassword(encodePassword(credentials.getPassword()));
              subscriber.onNext(credentials);
            } catch (NoSuchAlgorithmException e) {
              subscriber.onError(e);
            } finally {
              subscriber.onCompleted();
            }
          }
        });
      }
    }).flatMap(new Func1<Credentials, Observable<Token>>() {
      @Override
      public Observable<Token> call(Credentials credentials) {

        return api
            .getToken(RegistrationApi.authHeader, credentials.getLogin(), credentials.getPassword(),
                      "password", "read write", "123456", "clientapp");
      }
    }).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public static String encodePassword(String password) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("SHA-256");
    byte[] digest = md.digest(password.getBytes());
    return Base64.encodeToString(digest, Base64.URL_SAFE | Base64.NO_PADDING | Base64.NO_WRAP);
  }
}
