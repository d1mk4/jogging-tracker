package io.dp.jogging.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Singleton;

import timber.log.Timber;

/**
 * Created by dp on 07/09/14.
 */
@Singleton
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

  private static final String DATABASE_NAME = "jogging.db";
  private static final int DATABASE_VERSION = 1;

  private Context context;

  private Dao<Time, Long> timeDao = null;

  @Inject
  public DatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
    this.context = context;

  }

  @Override
  public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
    try {
      Timber.i("onCreate");
      TableUtils.createTableIfNotExists(connectionSource, Time.class);

      getTimesDao();
    } catch (SQLException e) {
      Timber.e(e, "Can't create database");
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion,
                        int newVersion) {
    Timber.i("onUpgrade");
    try {
      TableUtils.dropTable(connectionSource, Time.class, true);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    onCreate(database, connectionSource);
  }

  public Dao<Time, Long> getTimesDao() throws SQLException {
    if (timeDao == null) {
      timeDao = getDao(Time.class);
    }
    return timeDao;
  }
}
