package io.dp.jogging.net;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by dp on 14/09/14.
 */
public interface RegistrationApi {

  public final static String authHeader = "Basic " + Base64
      .encodeToString("clientapp:123456".getBytes(), Base64.DEFAULT);

  @FormUrlEncoded
  @POST("/signup")
  public void signup(@Field("username") String username, @Field("password") String password,
                     Callback<Response<User>> callback);

  @FormUrlEncoded
  @POST("/signup")
  public Observable<User> signup(@Field("username") String username,
                                 @Field("password") String password);

  @FormUrlEncoded
  @POST("/oauth/token")
  public void getToken(@Field("username") String username,
                       @Field("password") String password,
                       @Field("grant_type") String grantType,
                       @Field("scope") String scope,
                       @Field("client_secret") String clientSecret,
                       @Field("client_id") String clientId, Callback<Token> callback);

  @FormUrlEncoded
  @POST("/oauth/token")
  public Observable<Token> getToken(@Header("Authorization") String authorization,
                                    @Field("username") String username,
                                    @Field("password") String password,
                                    @Field("grant_type") String grantType,
                                    @Field("scope") String scope,
                                    @Field("client_secret") String clientSecret,
                                    @Field("client_id") String clientId);


}
