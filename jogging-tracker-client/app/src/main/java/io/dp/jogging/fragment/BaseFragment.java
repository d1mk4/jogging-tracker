package io.dp.jogging.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import io.dp.jogging.R;
import io.dp.jogging.activity.BaseActivity;
import io.dp.jogging.annotation.FragmentLayout;

/**
 * Created by dp on 07/09/14.
 */
public abstract class BaseFragment extends Fragment {

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    ((BaseActivity) getActivity()).inject(this);

    getActivity().setTitle(R.string.app_name);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    Class<?> targetClass = getClass();
    FragmentLayout layout = targetClass.getAnnotation(FragmentLayout.class);
    if (layout == null) {
      throw new RuntimeException("FragmentLayout should be defined under " + getClass().getName());
    }

    View v = inflater.inflate(layout.value(), container, false);
    ButterKnife.inject(this, v);
    return v;
  }
}
