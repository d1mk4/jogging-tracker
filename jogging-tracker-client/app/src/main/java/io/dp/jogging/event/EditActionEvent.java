package io.dp.jogging.event;

import io.dp.jogging.db.Time;

/**
 * Created by dp on 16/09/14.
 */
public class EditActionEvent {

  Time item;

  public EditActionEvent(Time item) {
    this.item = item;
  }

  public Time getTime() {
    return item;
  }
}
