package io.dp.jogging.event;

import retrofit.RetrofitError;

/**
 * Created by dp on 16/09/14.
 */
public class RestApiErrorEvent {

  RetrofitError error;

  public RestApiErrorEvent(RetrofitError error) {
    this.error = error;
  }

  public RetrofitError getError() {
    return error;
  }
}
