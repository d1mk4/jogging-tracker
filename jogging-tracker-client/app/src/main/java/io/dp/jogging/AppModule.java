package io.dp.jogging;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.dp.jogging.event.AsyncBus;
import io.dp.jogging.net.ApiModule;
import timber.log.Timber;

/**
 * Created by dp on 06/09/14.
 */
@Module(includes = { ApiModule.class },
        library = true)
public class AppModule {

  private Application application;

  public AppModule(Application application) {
    this.application = application;

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    } else {
      // release configuration
      // Timber.plant(new CrashReportingTree());
    }
  }

  @Singleton
  @Provides
  public Context provideContext() {
    return application;
  }

  @Singleton
  @Provides
  public Bus provideBus() {
    return new AsyncBus(ThreadEnforcer.ANY);
  }

  @Singleton
  @Provides
  public SharedPreferences provideSharedPreferences() {
    return this.application.getSharedPreferences("token", Context.MODE_PRIVATE);
  }
}
