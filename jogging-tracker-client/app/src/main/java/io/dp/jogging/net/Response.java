package io.dp.jogging.net;

/**
 * Created by dp on 07/09/14.
 */
public class Response<T> {
  private int status;
  private T data;

  public Response(int result, T data) {
    this.status = result;
    this.data = data;
  }

  public int getStatus() {
    return status;
  }

  public T getData() {
    return data;
  }
}
