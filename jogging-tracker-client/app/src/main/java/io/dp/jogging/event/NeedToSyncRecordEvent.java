package io.dp.jogging.event;

import io.dp.jogging.db.Time;

/**
 * Created by dp on 07/09/14.
 */
public class NeedToSyncRecordEvent {

  Time time;

  public NeedToSyncRecordEvent(Time time) {
    this.time = time;
  }

  public Time getTime() {
    return time;
  }
}
