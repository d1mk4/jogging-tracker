package io.dp.jogging.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Bus;

import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.dp.jogging.R;
import io.dp.jogging.db.DatabaseHelper;
import io.dp.jogging.db.Time;
import io.dp.jogging.db.utils.OrmliteCursorAdapter;
import io.dp.jogging.db.utils.Queries;
import io.dp.jogging.event.DeleteActionEvent;
import io.dp.jogging.event.EditActionEvent;
import io.dp.jogging.event.NeedToSyncRecordEvent;

/**
 * Created by dp on 07/09/14.
 */
public class TimesOrmliteAdapter extends OrmliteCursorAdapter<Time> {

  private LayoutInflater inflater;
  private Bus bus;
  private Calendar calendar = Calendar.getInstance();
  private int range = 0;
  private int avgDistance = 0;
  private int avgSpeed = 0;

  @Inject
  public TimesOrmliteAdapter(Context context, DatabaseHelper dbHelper, Bus bus) {
    super(context, null, Queries.prepareTimeQuery(dbHelper, true, -1, -1));

    this.bus = bus;
    this.inflater = LayoutInflater.from(context);
  }

  private static String formatAvg(Context context, int week, double avgDistance, double avgSpeed) {
    return context.getResources().getString(R.string.avg_distance_and_speed, week, avgDistance, avgSpeed);
  }

  @Override
  public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
    View v = this.inflater.inflate(R.layout.item_time, viewGroup, false);
    Holder holder = new Holder(v, bus);
    v.setTag(holder);
    return v;
  }

  @Override
  public void bindView(View v, Context context, Time item) {
    Holder holder = (Holder) v.getTag();

    holder.item = item;
    double t = item.getTime();

    holder.distanceView.setText(context.getResources().getString(R.string.distance_m, item.getDistance()));
    holder.timeView.setText(String.format(Locale.US, "%d:%02d:%02d", (int) (t / 60. / 60.), (int) (t / 60. % 60), (int) (t % 60)));

    holder.dateTimeView
        .setText(DateUtils.formatDateTime(context, item.getDate(), DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));

    holder.avgSpeedView
        .setText(t > 0 ? String.format(Locale.US, "%.2f m/s", item.getDistance() / t) : "");

    UUID uuid = item.getServerId();
    if (uuid == null || item.getNeedSync() > 0) {
      holder.syncProgressView.setVisibility(item.isSyncing() ? View.VISIBLE : View.GONE);
      holder.needSyncView.setVisibility(View.VISIBLE);
    } else {
      holder.needSyncView.setVisibility(View.GONE);
      holder.syncProgressView.setVisibility(View.GONE);
    }

    avgDistance += item.getDistance();
    avgSpeed += item.getDistance() / t;

    range++;

    calendar.setTimeInMillis(item.getDate());
    int currentWeek = calendar.get(Calendar.WEEK_OF_YEAR);

    holder.weekHeaderView.setVisibility(View.GONE);
    int currentPos = getCursor().getPosition();
    int pos = currentPos + 1;
    if (pos >= 0 && pos < getCursor().getCount()) {
      Time nextTime = getItem(pos);

      calendar.setTimeInMillis(nextTime.getDate());
      int nextWeek = calendar.get(Calendar.WEEK_OF_YEAR);

      if (currentWeek != nextWeek && range > 0) {
        holder.weekHeaderView.setVisibility(View.VISIBLE);
        holder.weekHeaderView
            .setText(formatAvg(context, currentWeek, avgDistance / (double) range, avgSpeed / (double) range));

        avgDistance = 0;
        avgSpeed = 0;
        range = 0;
      }
    } else if (pos >= getCursor().getCount()) {
      holder.weekHeaderView.setVisibility(View.VISIBLE);
      holder.weekHeaderView
          .setText(formatAvg(context, currentWeek, avgDistance / (double) range, avgSpeed / (double) range));

      avgDistance = 0;
      avgSpeed = 0;
      range = 0;
    }
  }

  static class Holder {

    @InjectView(R.id.datetime)
    TextView dateTimeView;

    @InjectView(R.id.avg_speed)
    TextView avgSpeedView;

    @InjectView(R.id.need_sync)
    ImageView needSyncView;

    @InjectView(R.id.sync_progress)
    ProgressBar syncProgressView;

    @InjectView(R.id.week_header)
    TextView weekHeaderView;

    @InjectView(R.id.distance)
    TextView distanceView;

    @InjectView(R.id.time)
    TextView timeView;

    Bus bus;

    Time item = null;

    public Holder(View v, Bus bus) {
      ButterKnife.inject(this, v);

      this.bus = bus;
    }

    @OnClick(R.id.need_sync)
    public void onNeedSyncClick(View v) {
      if (item != null) {
        item.setSyncing(true);
      }

      needSyncView.setVisibility(View.GONE);
      syncProgressView.setVisibility(View.VISIBLE);

      bus.post(new NeedToSyncRecordEvent(item));
    }

    @OnClick(R.id.action_delete)
    public void onActinDeleteClick(View v) {
      bus.post(new DeleteActionEvent(item));
    }

    @OnClick(R.id.action_edit)
    public void onActinEditClick(View v) {
      bus.post(new EditActionEvent(item));
    }
  }
}
