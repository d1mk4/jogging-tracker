package io.dp.jogging.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.InjectView;
import io.dp.jogging.Const;
import io.dp.jogging.R;
import io.dp.jogging.activity.LoginActivity;
import io.dp.jogging.adapter.TimesOrmliteAdapter;
import io.dp.jogging.annotation.FragmentLayout;
import io.dp.jogging.db.DatabaseHelper;
import io.dp.jogging.db.Time;
import io.dp.jogging.db.utils.OrmliteCursorLoader;
import io.dp.jogging.db.utils.Queries;
import io.dp.jogging.event.DateRangeEvent;
import io.dp.jogging.event.DeleteActionEvent;
import io.dp.jogging.event.EditActionEvent;
import io.dp.jogging.event.NeedToSyncRecordEvent;
import io.dp.jogging.event.RestApiErrorEvent;
import io.dp.jogging.event.UnauthorizedAccessEvent;
import io.dp.jogging.event.UpdateTimesListEvent;
import io.dp.jogging.net.Response;
import io.dp.jogging.net.RestApi;
import retrofit.Callback;
import retrofit.RetrofitError;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by dp on 07/09/14.
 */

@FragmentLayout(R.layout.fragment_times)
public class TimesFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>,
                                                           Observer<Boolean>,
                                                           SwipeRefreshLayout.OnRefreshListener {

  public static final String ORDER = "order";
  public static final String START_DATE = "startDate";
  public static final String FINISH_DATE = "finishDate";

  @Inject
  TimesOrmliteAdapter adapter;

  @Inject
  DatabaseHelper dbHelper;

  @Inject
  Bus bus;

  @Inject
  RestApi api;

  @Inject
  SharedPreferences prefs;

  @InjectView(R.id.times_list)
  ListView timesListView;

  @InjectView(R.id.swipe_layout)
  SwipeRefreshLayout swipeRefreshView;

  @InjectView(R.id.empty)
  TextView emptyView;

  boolean isDestroyed = false;

  List<Subscription> subscriptionList = new ArrayList<Subscription>();

  Bundle sortArgs = new Bundle();

  CalendarDialogFragment calendarDialog = new CalendarDialogFragment();

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    timesListView.setAdapter(adapter);
    timesListView.setEmptyView(emptyView);
    setHasOptionsMenu(true);

    isDestroyed = false;

    sortArgs.putBoolean(ORDER, false);

    swipeRefreshView.setOnRefreshListener(this);
    swipeRefreshView.setColorSchemeResources(R.color.apptheme_color, R.color.refresh_color_1, R.color.refresh_color_2, R.color.refresh_color_3);

    refresh();
  }

  @Override
  public void onDestroyView() {
    for (Subscription s : subscriptionList) {
      if (s != null) {
        s.unsubscribe();
      }
    }

    isDestroyed = true;

    super.onDestroyView();
  }

  @Override
  public void onResume() {
    super.onResume();
    bus.register(this);

    bus.post(new UpdateTimesListEvent());
  }

  @Override
  public void onPause() {
    super.onPause();
    bus.unregister(this);
  }

  @Subscribe
  public void onUnathorizedAccess(UnauthorizedAccessEvent event) {
    Toast.makeText(getActivity(), R.string.token_is_not_valid, Toast.LENGTH_SHORT)
        .show();
    startActivity(new Intent(getActivity(), LoginActivity.class));
    getActivity().finish();
  }

  @Subscribe
  public void onNetworkError(RestApiErrorEvent event) {
    Toast.makeText(getActivity(), event.getError().getLocalizedMessage(), Toast.LENGTH_SHORT)
        .show();
  }

  @Subscribe
  public void onDeleteAction(DeleteActionEvent event) {

    final Time time = event.getTime();

    Timber.v("! onDeleteAction " + time);

    final Observable<Boolean> deleteObservable = Observable.create(
        new Observable.OnSubscribe<Boolean>() {
          @Override
          public void call(
              Subscriber<? super Boolean> subscriber) {
            try {
              dbHelper.getTimesDao()
                  .deleteById(time.getTimeId());
              subscriber.onNext(true);
            } catch (SQLException e) {
              e.printStackTrace();
              subscriber.onError(e);
            } finally {
              subscriber.onCompleted();
            }
          }
        });

    if (time != null) {
      UUID serverUuid = time.getServerId();

      Subscription s;
      if (serverUuid != null) {
        s =
            AndroidObservable.bindFragment(this,
                                           api.deleteTime(serverUuid.toString())
                                               .flatMap(
                                                   new Func1<Response<Boolean>, Observable<Boolean>>() {
                                                     @Override
                                                     public Observable<Boolean> call(
                                                         Response<Boolean> booleanResponse) {
                                                       return deleteObservable;
                                                     }
                                                   })).observeOn(Schedulers.io())
                                               .subscribe(this);

      } else {
        s =
            AndroidObservable.bindFragment(this, deleteObservable).observeOn(Schedulers.io())
                .subscribe(this);
      }

      subscriptionList.add(s);
    }
  }

  @Subscribe
  public void onEditAction(EditActionEvent event) {
    final Time time = event.getTime();
    getFragmentManager().beginTransaction()
        .replace(R.id.container, AddDateTimeFragment.newInstance(time.getTimeId()))
        .addToBackStack(null)
        .commit();
  }

  @Subscribe
  public void onUpdateTimesList(UpdateTimesListEvent event) {
    getLoaderManager().restartLoader(0, sortArgs, this);
  }

  @Subscribe
  public void onDateRange(DateRangeEvent event) {
    Timber.v("Got: " + event.getFirstDate());

    sortArgs.putLong(START_DATE, event.getFirstDate().getTime());
    sortArgs.putLong(FINISH_DATE, event.getLastDate().getTime());
    getLoaderManager().restartLoader(0, sortArgs, this);
  }

  @Subscribe
  public void onNeedToSync(NeedToSyncRecordEvent event) {
    final Time time = event.getTime();
    if(time.getNeedSync() > 0) {
      api.updateTime(time.getServerId().toString(), time.getTime(), time.getDistance(), time.getDate(),
                     new Callback<Response<Time>>() {
                       @Override
                       public void success(Response<Time> resp,
                                           retrofit.client.Response response) {
                         if (resp.getStatus() == 0) {
                           try {
                             UpdateBuilder<Time, Long>
                                 ub =
                                 dbHelper.getTimesDao().updateBuilder();
                             ub.updateColumnValue(Time.NEED_SYNC, 0)
                                 .where().idEq(time.getTimeId());
                             ub.update();

                             bus.post(new UpdateTimesListEvent());
                             time.setSyncing(false);
                           } catch (SQLException e) {
                             e.printStackTrace();
                           }
                         }
                       }

                       @Override
                       public void failure(RetrofitError error) {
                         time.setSyncing(false);
                         bus.post(new UpdateTimesListEvent());
                       }
                     });
    } else {
      api.postTimeRecord((int) time.getTimeId(), time.getTime(), time.getDistance(), time.getDate(),
                         new Callback<Response<Time>>() {
                           @Override
                           public void success(Response<Time> resp,
                                               retrofit.client.Response response) {

                             if (resp.getStatus() == 0) {
                               long timeId = resp.getData().getTimeId();
                               try {
                                 UpdateBuilder<Time, Long>
                                     ub =
                                     dbHelper.getTimesDao().updateBuilder();
                                 ub.updateColumnValue(Time.SERVER_ID, resp.getData().getServerId())
                                     .where().idEq(timeId);
                                 ub.update();

                                 bus.post(new UpdateTimesListEvent());
                               } catch (SQLException e) {
                                 e.printStackTrace();
                               }
                             }

                             time.setSyncing(false);
                           }

                           @Override
                           public void failure(RetrofitError error) {
                             time.setSyncing(false);
                             bus.post(new UpdateTimesListEvent());
                           }
                         });
    }

  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.times, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
      case R.id.action_exit:
        Subscription s = AndroidObservable.bindFragment(this, Observable.create(
            new Observable.OnSubscribe<Void>() {
              @Override
              public void call(Subscriber<? super Void> subscriber) {
                try {
                  DeleteBuilder<Time, Long> db = dbHelper.getTimesDao().deleteBuilder();
                  db.where().gt(Time.TIME_ID, -1);
                  db.delete();
                  subscriber.onNext(null);
                } catch (SQLException e) {
                  e.printStackTrace();
                  subscriber.onError(e);
                } finally {
                  prefs.edit().putString(Const.ACCESS_TOKEN, null).apply();
                  subscriber.onCompleted();
                }
              }
            })).observeOn(Schedulers.io())
            .subscribe(new Action1<Void>() {
              @Override
              public void call(Void aVoid) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
              }
            });
        subscriptionList.add(s);
        return true;

      case R.id.action_add:
        getFragmentManager().beginTransaction()
            .replace(R.id.container, AddDateTimeFragment.newInstance(-1))
            .addToBackStack(null)
            .commit();
        return true;

      case R.id.action_sort:
        boolean isChecked = !item.isChecked();
        item.setIcon(isChecked ? R.drawable.ic_sort_desc : R.drawable.ic_sort_asc);
        item.setChecked(isChecked);
        sortArgs.putBoolean(ORDER, isChecked);
        bus.post(new UpdateTimesListEvent());
        return true;

      case R.id.action_filter:
        calendarDialog.show(getFragmentManager(), CalendarDialogFragment.class.getSimpleName());
        return true;

      case R.id.action_reset:
        sortArgs.putLong(START_DATE, -1);
        sortArgs.putLong(FINISH_DATE, -1);
        bus.post(new UpdateTimesListEvent());
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public Loader<Cursor> onCreateLoader(int i, Bundle b) {
    try {
      boolean order = b.getBoolean(ORDER, false);

      long startDate = b.getLong(START_DATE, -1);
      long finishDate = b.getLong(FINISH_DATE, -1);

      PreparedQuery<Time> pq = Queries.prepareTimeQuery(dbHelper, order, startDate, finishDate);
      adapter.setQuery(pq);

      return new OrmliteCursorLoader<Time>(getActivity(), dbHelper.getTimesDao(), pq);
    } catch (SQLException e) {
      Timber.e(e, "Error to load times");
    }

    return null;
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor cursor){
      adapter.changeCursor(cursor);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {
      adapter.changeCursor(null);
  }

  @Override
  public void onCompleted() {
    Timber.v("Get time competed!");

    swipeRefreshView.setRefreshing(false);
  }

  @Override
  public void onError(Throwable e) {
    Timber.e(e, "Got error here!");
    swipeRefreshView.setRefreshing(false);
  }

  @Override
  public void onNext(Boolean doUpdate) {
    if (doUpdate != null && doUpdate) {
      bus.post(new UpdateTimesListEvent());
    }
  }

  @Override
  public void onRefresh() {
    refresh();
  }

  private void refresh() {
    swipeRefreshView.setRefreshing(true);
    Subscription
        subscription =
        AndroidObservable.bindFragment(this, api.getTimes().flatMap(
            new Func1<Response<List<Time>>, Observable<Boolean>>() {
              @Override
              public Observable<Boolean> call(final Response<List<Time>> listResponse) {
                return Observable.create(new Observable.OnSubscribe<Boolean>() {
                  @Override
                  public void call(Subscriber<? super Boolean> subscriber) {
                    try {
                      if (listResponse != null) {
                        Dao<Time, Long> dao = dbHelper.getTimesDao();
                        List<Time> data = listResponse.getData();
                        if (data != null && data.size() > 0) {
                          for (Time t : data) {
                            Timber.v("Time: " + t);
                            try {
                              dao.createIfNotExists(t);
                            } catch (SQLException ignored) {
                            }
                          }
                          subscriber.onNext(true);
                        }
                      }
                    } catch (Exception e) {
                      subscriber.onError(e);
                    } finally {
                      subscriber.onCompleted();
                    }
                  }
                });
              }
            })).subscribeOn(Schedulers.io()).subscribe(this);

    subscriptionList.add(subscription);
  }
}
