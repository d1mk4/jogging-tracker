package io.dp.jogging.net;

/**
 * Created by dp on 14/09/14.
 */
public class Credentials {

  private String login;
  private String password;

  public Credentials(String login, String password) {
    this.login = login;
    this.password = password;
  }

  public String getLogin() {
    return login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
