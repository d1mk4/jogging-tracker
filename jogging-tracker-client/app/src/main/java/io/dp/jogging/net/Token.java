package io.dp.jogging.net;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dp on 14/09/14.
 */
public class Token {
  @SerializedName("access_token")
  String accessToken;

  @SerializedName("token_type")
  String tokenType;

  @SerializedName("refresh_token")
  String refreshToken;

  @SerializedName("expires_in")
  Integer expiresIn;

  @SerializedName("scope")
  String scope;

  public String getAccessToken() {
    return accessToken;
  }

  public String getTokenType() {
    return tokenType;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public Integer getExpiresIn() {
    return expiresIn;
  }

  public String getScope() {
    return scope;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public void setExpiresIn(Integer expiresIn) {
    this.expiresIn = expiresIn;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }
}
