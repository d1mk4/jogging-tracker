package io.dp.jogging;

import android.app.Activity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.dp.jogging.activity.LoginActivity;
import io.dp.jogging.activity.MainActivity;
import io.dp.jogging.fragment.AddDateTimeFragment;
import io.dp.jogging.fragment.AddTimeDistanceFragment;
import io.dp.jogging.fragment.CalendarDialogFragment;
import io.dp.jogging.fragment.TimesFragment;

/**
 * Created by dp on 06/09/14.
 */
@Module(
    injects = {
        MainActivity.class,
        TimesFragment.class,
        AddDateTimeFragment.class,
        AddTimeDistanceFragment.class,
        LoginActivity.class,
        CalendarDialogFragment.class
    },
    addsTo = AppModule.class,
    library = true)
public class UiModule {

  private Activity activity;

  public UiModule(Activity activity) {
    this.activity = activity;
  }

  @Singleton
  @Provides
  public Activity provideActivity() {
    return this.activity;
  }
}
