package io.dp.jogging.event;

import retrofit.RetrofitError;

/**
 * Created by dp on 16/09/14.
 */
public class UnauthorizedAccessEvent {

  RetrofitError error;

  public UnauthorizedAccessEvent(RetrofitError error) {
    this.error = error;
  }

  public RetrofitError getError() {
    return error;
  }
}
