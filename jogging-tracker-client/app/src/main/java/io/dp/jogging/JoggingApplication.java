package io.dp.jogging;

import android.app.Application;
import android.os.StrictMode;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;
import timber.log.Timber;

/**
 * Created by dp on 06/09/14.
 */
public class JoggingApplication extends Application {

  private ObjectGraph objectGraph;

  public void setObjectGraph(ObjectGraph objectGraph) {
    this.objectGraph = objectGraph;
  }

  @Override
  public void onCreate() {
    super.onCreate();

    createObjectGraph();

    strictMode();
  }

  protected void createObjectGraph() {
    setObjectGraph(ObjectGraph.create(getModules().toArray()));
  }

  public boolean isTesting() {
    return false;
  }

  protected List<Object> getModules() {
    return Arrays.<Object>asList(new AppModule(this));
  }

  public ObjectGraph getApplicationGraph() {
    return objectGraph;
  }

  private void strictMode() {
    if (BuildConfig.DEBUG && !isTesting()) {
      Timber.d("Strict mode is enabled");
      StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                                     .detectAll()
                                     .penaltyLog()
                                     .build());
      StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                                 .detectAll()
                                 .penaltyLog()
                                 .build());
    }
  }
}
