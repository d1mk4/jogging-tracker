package io.dp.jogging.net;

import java.util.List;

import io.dp.jogging.db.Time;
import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by dp on 07/09/14.
 */
public interface RestApi {

  @GET("/tracks")
  public Observable<Response<List<Time>>> getTimes();

  @DELETE("/tracks/{id}")
  public Observable<Response<Boolean>> deleteTime(@Path("id") String serverId);

  @FormUrlEncoded
  @PUT("/tracks/{id}")
  public Observable<Response<Time>> updateTime(@Path("id") String serverId, @Field("time") int time,
                                               @Field("distance") int distance,
                                               @Field("date") long date);

  @FormUrlEncoded
  @POST("/tracks/{id}")
  public void updateTime(@Path("id") String serverId, @Field("time") int time,
                                               @Field("distance") int distance,
                                               @Field("date") long date, Callback<Response<Time>> callback);

  @FormUrlEncoded
  @POST("/tracks")
  public void postTimeRecord(@Field("id") int id, @Field("time") int time, @Field("distance") int distance,
                             @Field("date") long date, Callback<Response<Time>> callback);

  @FormUrlEncoded
  @POST("/tracks")
  public Observable<Response<Time>> postTimeRecord(@Field("id") int id, @Field("time") int time, @Field("distance") int distance,
                             @Field("date") long date);
}
