package io.dp.jogging.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import dagger.ObjectGraph;
import io.dp.jogging.JoggingApplication;
import io.dp.jogging.UiModule;

/**
 * Created by dp on 06/09/14.
 */
public abstract class BaseActivity extends ActionBarActivity {

  private ObjectGraph activityGraph;
  private UiModule uiModule;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    JoggingApplication application = (JoggingApplication) getApplication();
    uiModule = new UiModule(this);
    activityGraph = application.getApplicationGraph().plus(uiModule);

    activityGraph.inject(this);

    super.onCreate(savedInstanceState);
  }

  /**
   * Inject the supplied {@code object} using the activity-specific graph.
   */
  public void inject(Object object) {
    if (activityGraph != null) {
      activityGraph.inject(object);
    }
  }
}
