package io.dp.jogging.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.squareup.timessquare.CalendarPickerView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.dp.jogging.R;
import io.dp.jogging.activity.BaseActivity;
import io.dp.jogging.event.DateRangeEvent;

/**
 * Created by dp on 18/09/14.
 */
public class CalendarDialogFragment extends DialogFragment {

  @Inject
  Bus bus;

  @InjectView(R.id.calendar_view)
  CalendarPickerView calendarView;

  @OnClick(R.id.done)
  public void onDoneClick(View v) {
    List<Date> dates = calendarView.getSelectedDates();

    if (dates.size() > 0) {
      Date firstDate = dates.get(0);
      Date lastDate = dates.get(dates.size() - 1);

      bus.post(new DateRangeEvent(firstDate, lastDate));
    }

    dismiss();
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    Dialog dlg = super.onCreateDialog(savedInstanceState);
    dlg.setTitle(R.string.enter_range_to_filter_tracks);

    ((BaseActivity) getActivity()).inject(this);

    return dlg;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_calendar_filter, container, false);
    ButterKnife.inject(this, v);

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);

    Date today = new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000);
    calendarView.init(calendar.getTime(), today)
        .inMode(CalendarPickerView.SelectionMode.RANGE);
    return v;
  }
}
