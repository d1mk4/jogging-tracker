package io.dp.jogging.db;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;

/**
 * Created by dp on 07/09/14.
 */
@DatabaseTable(tableName = "times")
public class Time {

  public static final String TIME_ID = "time_id";
  public static final String SERVER_ID = "server_id";
  public static final String DISTANCE = "distance";
  public static final String TIME = "time";
  public static final String DATE = "date";
  public static final String NEED_SYNC = "need_sync";

  boolean isSyncing = false;

  @DatabaseField(generatedId = true, dataType = DataType.LONG, columnName = TIME_ID)
  private long timeId;

  @DatabaseField(unique = true, dataType = DataType.UUID, columnName = SERVER_ID)
  private UUID serverId;

  @DatabaseField(dataType = DataType.INTEGER, columnName = DISTANCE)
  private int distance;

  @DatabaseField(dataType = DataType.INTEGER, columnName = TIME)
  private int time;

  @DatabaseField(dataType = DataType.LONG, columnName = DATE)
  private long date;

  @DatabaseField(dataType = DataType.INTEGER, columnName = NEED_SYNC, defaultValue = "0")
  private int needSync;

  public void setNeedSync(int needSync) {
    this.needSync = needSync;
  }

  public Time() {
  }

  public boolean isSyncing() {
    return isSyncing;
  }

  public void setSyncing(boolean isSyncing) {
    this.isSyncing = isSyncing;
  }

  public Time(int distance, int time, long date) {
    this.distance = distance;
    this.time = time;
    this.date = date;
  }

  public long getTimeId() {
    return timeId;
  }

  public UUID getServerId() {
    return serverId;
  }

  public int getDistance() {
    return distance;
  }

  public int getTime() {
    return time;
  }

  public long getDate() {
    return date;
  }

  public int getNeedSync() {
    return needSync;
  }

  @Override
  public String toString() {
    return "Time{" +
           "timeId=" + timeId +
           ", serverId=" + serverId +
           ", distance=" + distance +
           ", time=" + time +
           ", date=" + date +
           '}';
  }
}
