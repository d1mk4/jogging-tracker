package io.dp.jogging.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.dp.jogging.Const;
import io.dp.jogging.R;
import io.dp.jogging.net.Credentials;
import io.dp.jogging.net.RegistrationApi;
import io.dp.jogging.net.Token;
import io.dp.jogging.net.User;
import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import timber.log.Timber;

public class LoginActivity extends BaseActivity implements Observer<Token>{

  @Inject
  SharedPreferences prefs;

  @Inject
  RegistrationApi api;

  @InjectView(R.id.login)
  TextView loginView;

  @InjectView(R.id.password)
  TextView passwordView;

  @InjectView(R.id.progress)
  ProgressBar progressBarView;

  List<Subscription> subscriptionList = new ArrayList<Subscription>();

  @OnClick(R.id.do_login)
  public void onDoLoginClick(View v) {
    String login = loginView.getText().toString();
    String password = passwordView.getText().toString();

    if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)) {
      Credentials c = new Credentials(login, password);

      progressBarView.setVisibility(View.VISIBLE);
      subscriptionList.add(AndroidObservable.bindActivity(this, User.login(c, api)).subscribe(this));
    }
  }

  @OnClick(R.id.do_signup)
  public void onDoSignupClick(View v) {
    String login = loginView.getText().toString();
    String password = passwordView.getText().toString();

    if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)) {
      Credentials c = new Credentials(login, password);

      progressBarView.setVisibility(View.VISIBLE);
      subscriptionList.add(AndroidObservable.bindActivity(this, User.signUp(c, api)).subscribe(this));
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    String accessToken = prefs.getString(Const.ACCESS_TOKEN, null);
    if (!TextUtils.isEmpty(accessToken)) {
      startActivity(new Intent(this, MainActivity.class));
      finish();
    }

    setContentView(R.layout.activity_login);
    ButterKnife.inject(this);
  }

  @Override
  protected void onDestroy() {
    for (Subscription s : subscriptionList) {
      if (s != null) {
        s.unsubscribe();
      }
    }

    super.onDestroy();
  }

  @Override
  public void onCompleted() {
    Timber.v("onCompleted");

    progressBarView.setVisibility(View.GONE);
  }

  @Override
  public void onError(Throwable e) {
    Toast.makeText(this, R.string.error_to_authenticate, Toast.LENGTH_SHORT).show();
    Timber.e(e, "Error to authenticate");

    progressBarView.setVisibility(View.GONE);
  }

  @Override
  public void onNext(Token token) {
    String accessToken = token.getAccessToken();
    if (!TextUtils.isEmpty(accessToken)) {
      prefs.edit().putString(Const.ACCESS_TOKEN, accessToken).apply();
      startActivity(new Intent(LoginActivity.this, MainActivity.class));
      finish();
    }
  }
}
