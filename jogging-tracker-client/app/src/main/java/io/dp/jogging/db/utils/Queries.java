package io.dp.jogging.db.utils;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;

import io.dp.jogging.db.DatabaseHelper;
import io.dp.jogging.db.Time;
import timber.log.Timber;

/**
 * Created by dp on 07/09/14.
 */
public class Queries {

  public static PreparedQuery<Time> prepareTimeQuery(DatabaseHelper dbHelper, boolean asc, long startDate, long finishDate) {
    try {
      QueryBuilder<Time, Long> qb = dbHelper.getTimesDao().queryBuilder();

      if (startDate > 0 && finishDate > 0 && startDate < finishDate) {
        Timber.v("Try to select: " + startDate + " " + finishDate);
        qb.where().ge(Time.DATE, startDate).and().le(Time.DATE, finishDate);
      }

      qb.orderBy(Time.DATE, asc);

      return qb.prepare();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return null;
  }
}
