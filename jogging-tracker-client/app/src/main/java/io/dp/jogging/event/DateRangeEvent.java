package io.dp.jogging.event;

import java.util.Date;

/**
 * Created by dp on 18/09/14.
 */
public class DateRangeEvent {

  Date firstDate;
  Date lastDate;

  public DateRangeEvent(Date firstDate, Date lastDate) {
    this.firstDate = firstDate;
    this.lastDate = lastDate;
  }

  public Date getFirstDate() {
    return firstDate;
  }

  public Date getLastDate() {
    return lastDate;
  }
}
