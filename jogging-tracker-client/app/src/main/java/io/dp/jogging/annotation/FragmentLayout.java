package io.dp.jogging.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by dp on 07/09/14.
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface FragmentLayout {

  /**
   * View ID to which the field will be bound.
   */
  int value();
}
