package io.dp.jogging.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import io.dp.jogging.R;
import io.dp.jogging.annotation.FragmentLayout;
import io.dp.jogging.db.DatabaseHelper;
import io.dp.jogging.db.Time;
import io.dp.jogging.net.Response;
import io.dp.jogging.net.RestApi;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by dp on 07/09/14.
 */
@FragmentLayout(R.layout.fragment_add_time_distance)
public class AddTimeDistanceFragment extends BaseFragment {

  @InjectView(R.id.distance)
  EditText distanceView;

  @InjectView(R.id.hours)
  EditText hoursView;

  @InjectView(R.id.minutes)
  EditText minutesView;

  @InjectView(R.id.seconds)
  EditText secondsView;

  @Inject
  DatabaseHelper dbHelper;

  @Inject
  RestApi api;

  private long dateTimeMillis;

  public static AddTimeDistanceFragment newInstance(long timeId, long date) {
    Bundle args = new Bundle();
    args.putLong(Time.TIME_ID, timeId);
    args.putLong(Time.DATE, date);
    AddTimeDistanceFragment f = new AddTimeDistanceFragment();
    f.setArguments(args);
    return f;
  }

  @OnClick(R.id.done)
  public void onDoneClick(View v) {
    String distanceStr = distanceView.getText().toString();

    String hoursStr = hoursView.getText().toString();
    String minsStr = minutesView.getText().toString();
    String secondsStr = secondsView.getText().toString();

    try {
      final int distance = Integer.valueOf(distanceStr);

      int hours = TextUtils.isEmpty(hoursStr) ? 0 : Integer.valueOf(hoursStr);
      int mins = TextUtils.isEmpty(minsStr) ? 0 : Integer.valueOf(minsStr);
      int seconds = TextUtils.isEmpty(secondsStr) ? 0 : Integer.valueOf(secondsStr);

      final int time = hours * 60 * 60 + mins * 60 + seconds;

      long timeId = getArguments().getLong(Time.TIME_ID, -1);
      if (timeId > -1)  {
        UpdateBuilder<Time, Long> ub = dbHelper.getTimesDao().updateBuilder();
        ub.updateColumnValue(Time.DISTANCE, distance);
        ub.updateColumnValue(Time.TIME, time);
        ub.updateColumnValue(Time.DATE, dateTimeMillis);

        ub.updateColumnValue(Time.NEED_SYNC, 1);
        ub.where().idEq(timeId);
        ub.update();
      } else {
        dbHelper.getTimesDao().create(new Time(distance, time, dateTimeMillis));
      }

      Fragment f = getFragmentManager().findFragmentByTag(TimesFragment.class.getSimpleName());
      getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
      getFragmentManager().beginTransaction()
          .replace(R.id.container, f)
          .commit();

    } catch (NumberFormatException e) {
      Toast.makeText(getActivity(), R.string.error_input_values, Toast.LENGTH_SHORT).show();
      Timber.e(e, "problem to convert string");
    } catch (SQLException e) {
      Timber.e(e, "problem sql");
    }
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    Bundle b = getArguments();
    this.dateTimeMillis = b.getLong(Time.DATE, System.currentTimeMillis());

    long timeId = b.getLong(Time.TIME_ID, -1);

    if (timeId > -1) {
      try {
        Time t = dbHelper.getTimesDao().queryForId(timeId);

        int time = t.getTime();
        hoursView.setText(String.valueOf((int) (time / 60. / 60.)));
        minutesView.setText(String.valueOf((int) (time / 60. % 60)));
        secondsView.setText(String.valueOf(time % 60));
        distanceView.setText(t.getDistance() + "");
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    getActivity().setTitle(R.string.title_fill_distance_time);
  }
}
