package io.dp.jogging.fragment;

import com.squareup.otto.Bus;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import javax.inject.Inject;

import io.dp.jogging.TestJoggingApplication;
import io.dp.jogging.db.Time;
import io.dp.jogging.net.Response;
import io.dp.jogging.net.RestApi;
import rx.Observer;

import static org.junit.Assert.assertTrue;

@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class TimesFragmentTest {

  @Inject
  Bus bus;

  @Inject
  RestApi restApi;

  @Before
  public void setUp() throws Exception {
    ((TestJoggingApplication) Robolectric.application).getApplicationGraph().inject(this);
  }

  @Test
  public void testOnNeedToSync() throws Exception {
    restApi.getTimes().subscribe(new Observer<Response<List<Time>>>() {
      @Override
      public void onCompleted() {

      }

      @Override
      public void onError(Throwable e) {

      }

      @Override
      public void onNext(Response<List<Time>> listResponse) {
        assertTrue(listResponse != null);
        assertTrue(listResponse.getData() != null);
        assertTrue(listResponse.getData().size() > 0);
      }
    });
  }
}