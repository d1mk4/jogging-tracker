package io.dp.jogging.net;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import javax.inject.Inject;

import io.dp.jogging.TestJoggingApplication;
import io.dp.jogging.db.Time;
import rx.Observer;
import rx.functions.Action1;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class RestApiTest {

  @Inject
  RestApi api;

  @Before
  public void setUp() throws Exception {
    ((TestJoggingApplication) Robolectric.application).getApplicationGraph().inject(this);
  }

  @Test
  public void testGetTimes() throws Exception {
    api.getTimes().subscribe(new Action1<Response<List<Time>>>() {
      @Override
      public void call(Response<List<Time>> listResponse) {
        assertTrue(listResponse != null);
        assertTrue(listResponse.getData() != null);
        assertTrue(listResponse.getData().size() > 0);
      }
    });

    Robolectric.runBackgroundTasks();
  }


  @Test
  public void testDeleteTime() throws Exception {
    api.deleteTime("1111").subscribe(new Observer<Response<Boolean>>() {
      @Override
      public void onCompleted() {

      }

      @Override
      public void onError(Throwable e) {

      }

      @Override
      public void onNext(Response<Boolean> booleanResponse) {
        assertTrue(booleanResponse != null && booleanResponse.getData() != null && booleanResponse.getData());
      }
    });

    Robolectric.runBackgroundTasks();
  }


  @Test
  public void testUpdateTime() throws Exception {
    api.updateTime("1111", 111, 111, 123456789111L).subscribe(new Action1<Response<Time>>() {
      @Override
      public void call(Response<Time> timeResponse) {
        assertNotNull(timeResponse);
        assertTrue(timeResponse.getData() != null && timeResponse.getData().getDate() > 0);
      }
    });

    Robolectric.runBackgroundTasks();
  }

  @Test
  public void testPostTimeRecord() throws Exception {
    api.postTimeRecord(1, 111, 111, 123456789111L).subscribe(new Action1<Response<Time>>() {
      @Override
      public void call(Response<Time> timeResponse) {
        assertNotNull(timeResponse);
        assertTrue(timeResponse.getData() != null && timeResponse.getData().getDate() > 0);
      }
    });

    Robolectric.runBackgroundTasks();
  }
}