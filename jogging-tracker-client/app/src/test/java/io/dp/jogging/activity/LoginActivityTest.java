package io.dp.jogging.activity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import javax.inject.Inject;

import io.dp.jogging.TestJoggingApplication;
import io.dp.jogging.net.Credentials;
import io.dp.jogging.net.RegistrationApi;
import io.dp.jogging.net.Token;
import io.dp.jogging.net.User;
import rx.Subscriber;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Config(emulateSdk = 18)
@RunWith(RobolectricTestRunner.class)
public class LoginActivityTest {

  @Inject
  RegistrationApi api;

  @Before
  public void setUp() throws Exception {
    ((TestJoggingApplication) Robolectric.application).getApplicationGraph().inject(this);
  }

  @Test
  public void testDoSignup() throws Exception {
    User.signUp(new Credentials("user", "pass"), api).subscribe(new Subscriber<Token>() {
      @Override
      public void onCompleted() {
      }

      @Override
      public void onError(Throwable e) {
      }

      @Override
      public void onNext(Token token) {
        assertTrue(token != null);

        assertEquals(token.getAccessToken(), TestJoggingApplication.ACCESS_TOKEN);
      }
    });

    Robolectric.runBackgroundTasks();
  }

  @Test
  public void testDoLogin() throws Exception {
    User.login(new Credentials("user", "pass"), api).subscribe(new Subscriber<Token>() {
      @Override
      public void onCompleted() {

      }

      @Override
      public void onError(Throwable e) {

      }

      @Override
      public void onNext(Token token) {
        assertTrue(token != null);
        assertEquals(token.getAccessToken(), TestJoggingApplication.ACCESS_TOKEN);
      }
    });

    User.signUp(new Credentials("user", "pass"), api).subscribe(new Subscriber<Token>() {
      @Override
      public void onCompleted() {
      }

      @Override
      public void onError(Throwable e) {
      }

      @Override
      public void onNext(Token token) {
        assertTrue(token != null);
      }
    });

    Robolectric.runBackgroundTasks();
  }
}
