package io.dp.jogging;

import org.robolectric.Robolectric;
import org.robolectric.TestLifecycleApplication;

import java.lang.reflect.Method;

import dagger.ObjectGraph;

/**
 * Created by dp on 14/09/14.
 */
public class TestJoggingApplication extends JoggingApplication implements TestLifecycleApplication {

  public static final String ACCESS_TOKEN = "asdadsfsafasdfsdf";

  @Override
  protected void createObjectGraph() {
    System.setProperty("robolectric.logging","stderr");

    setObjectGraph(ObjectGraph.create(new MockAppModule(Robolectric.application)));
  }

  @Override
  public boolean isTesting() {
    return true;
  }

  @Override
  public void beforeTest(Method method) {
    System.setProperty("robolectric.logging","stdout");
  }

  @Override
  public void prepareTest(Object o) {

  }

  @Override
  public void afterTest(Method method) {

  }
}
