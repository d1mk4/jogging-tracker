package io.dp.jogging;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import org.robolectric.Robolectric;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.dp.jogging.activity.LoginActivityTest;
import io.dp.jogging.db.DatabaseHelper;
import io.dp.jogging.db.Time;
import io.dp.jogging.event.AsyncBus;
import io.dp.jogging.fragment.TimesFragmentTest;
import io.dp.jogging.net.ApiModule;
import io.dp.jogging.net.RegistrationApi;
import io.dp.jogging.net.Response;
import io.dp.jogging.net.RestApi;
import io.dp.jogging.net.RestApiTest;
import io.dp.jogging.net.Token;
import io.dp.jogging.net.User;
import retrofit.MockRestAdapter;
import retrofit.RestAdapter;
import rx.Observable;
import rx.Subscriber;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by dp on 14/09/14.
 */
@Module(overrides = true,
        injects = {LoginActivityTest.class, TimesFragmentTest.class, RestApiTest.class},
        includes = ApiModule.class,
        library = true)
public class MockAppModule {

  Application application;

  public MockAppModule(Application application) {
    this.application = application;
  }

  @Singleton
  @Provides
  public Context provideContext() {
    return Robolectric.application;
  }

  @Singleton
  @Provides
  public Bus provideBus() {
    return new AsyncBus(ThreadEnforcer.ANY);
  }

  @Singleton
  @Provides
  public SharedPreferences providePrefs() {
    return application.getSharedPreferences("asdf", Context.MODE_PRIVATE);
  }

  @Provides
  public DatabaseHelper provideDatabaseHelper() {
    return OpenHelperManager.getHelper(application, DatabaseHelper.class);
  }

  @Provides
  @Singleton
  public Gson provideGson() {
    return new GsonBuilder().create();
  }

  @Singleton
  @Provides
  public RegistrationApi provideRegistrationApi(Bus bus, SharedPreferences prefs) {

    RestAdapter adapter = new RestAdapter.Builder().setEndpoint("http://test.com").build();

    RegistrationApi mockRegApi = mock(RegistrationApi.class);
    when(mockRegApi.signup("test", "password")).thenReturn(Observable.create(
        new Observable.OnSubscribe<User>() {
          @Override
          public void call(Subscriber<? super User> subscriber) {
            User u = new User();
            u.setUsername("test");
            subscriber.onNext(u);
            subscriber.onCompleted();
          }
        }));

    when(mockRegApi
        .getToken(RegistrationApi.authHeader, "test", "password", "password", "read write", "123456", "clientapp")).thenReturn(Observable.create(
        new Observable.OnSubscribe<Token>() {
          @Override
          public void call(Subscriber<? super Token> subscriber) {
            Token token = new Token();
            token.setAccessToken(TestJoggingApplication.ACCESS_TOKEN);
          }
        }));

    MockRestAdapter restAdapter = MockRestAdapter.from(adapter);
    return restAdapter.create(RegistrationApi.class, mockRegApi);
  }

  @Singleton
  @Provides
  public RestApi provideRestApi(Bus bus, SharedPreferences prefs) {

    RestAdapter adapter = new RestAdapter.Builder().setEndpoint("http://test.com").build();

    RestApi mockRestApi = mock(RestApi.class);
    when(mockRestApi.getTimes()).thenReturn(Observable.create(
        new Observable.OnSubscribe<Response<List<Time>>>() {
          @Override
          public void call(Subscriber<? super Response<List<Time>>> subscriber) {
            List<Time> l = new ArrayList<Time>();
            l.add(new Time(111, 11, 1111111111));
            subscriber.onNext(new Response<List<Time>>(0, l));
            subscriber.onCompleted();
          }
        }));

    when(mockRestApi.deleteTime("1111")).thenReturn(Observable.create(
        new Observable.OnSubscribe<Response<Boolean>>() {
          @Override
          public void call(Subscriber<? super Response<Boolean>> subscriber) {
            List<Time> l = new ArrayList<Time>();
            subscriber.onNext(new Response<Boolean>(0, true));
            subscriber.onCompleted();
          }
        }));

    when(mockRestApi.updateTime("1111", 111, 111, 123456789111L)).thenReturn(Observable.create(
        new Observable.OnSubscribe<Response<Time>>() {
          @Override
          public void call(Subscriber<? super Response<Time>> subscriber) {
            subscriber.onNext(new Response<Time>(0, new Time(111, 111, 123456789111L)));
            subscriber.onCompleted();
          }
        }));

    MockRestAdapter restAdapter = MockRestAdapter.from(adapter);
    return restAdapter.create(RestApi.class, mockRestApi);
  }
}
