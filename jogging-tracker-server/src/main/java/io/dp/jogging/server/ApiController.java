package io.dp.jogging.server;

import com.j256.ormlite.stmt.UpdateBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import io.dp.jogging.server.model.Response;
import io.dp.jogging.server.model.Time;
import io.dp.jogging.server.model.User;

/**
 * Created by dp on 07/09/14.
 */

@Secured({"ROLE_USER"})
@RestController
@RequestMapping("/api/v1")
public class ApiController {

  @Autowired
  private DatabaseHelper dbHelper;

  @RequestMapping(value = "/tracks", method = RequestMethod.POST)
  public Response<Time> putTrack(@RequestParam(value = "id", required = true) int id,
                                 @RequestParam(value = "distance", required = true) int distance,
                                 @RequestParam(value = "time", required = true) int time,
                                 @RequestParam(value = "date", required = true) long date) {

    Time result;
    try {
      UserDetails userDetails =
          (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

      User user = new User(userDetails.getUsername());

      result = dbHelper.getTimeDao().createIfNotExists(new Time(id, user, distance, time, date));
    } catch (SQLException e) {
      e.printStackTrace();
      return new Response<Time>(-1, null);
    }

    return new Response<Time>(0, result);
  }

  @RequestMapping(value = "/tracks", method = RequestMethod.GET)
  public Response<List<Time>> getTracksList() {

    List<Time> result;
    try {

      UserDetails userDetails =
          (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

      result = dbHelper.getTimeDao().queryForEq(User.USERNAME, new User(userDetails.getUsername()));
    } catch (SQLException e) {
      e.printStackTrace();
      return new Response<List<Time>>(-1, null);
    }

    return new Response<List<Time>>(0, result);
  }

  @RequestMapping(value = "/tracks/{id}", method = RequestMethod.DELETE)
  public Response<Boolean> deleteTime(@PathVariable("id") String serverId) {

    UUID id = UUID.fromString(serverId);

    try {
      int n = dbHelper.getTimeDao().deleteById(id);
      if (n > 0) {
        return new Response<Boolean>(0, true);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return new Response<Boolean>(-1, false);
  }

  @RequestMapping(value = "/tracks/{id}", method = RequestMethod.POST)
  public Response<Time> updateTime(@PathVariable("id") String serverId,
                                   @RequestParam(value = "distance", required = true) int distance,
                                   @RequestParam(value = "time", required = true) int time,
                                   @RequestParam(value = "date", required = true) long date) {

    UUID id = UUID.fromString(serverId);

    try {
      boolean doUpdate = false;
      UpdateBuilder<Time, UUID> ub = dbHelper.getTimeDao().updateBuilder();
      if (distance > 0) {
        doUpdate = true;
        ub.updateColumnValue(Time.DISTANCE, distance);
      }

      if (time > 0) {
        doUpdate = true;
        ub.updateColumnValue(Time.TIME, time);
      }

      if (date > 0) {
        doUpdate = true;
        ub.updateColumnValue(Time.DATE, date);
      }

      ub.where().idEq(id);
      if (doUpdate) {
        System.out.println("! updating");
        int n = ub.update();
        if (n > 0) {
          dbHelper.getTimeDao().clearObjectCache();
          return new Response<Time>(0, dbHelper.getTimeDao().queryForId(id));
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return new Response<Time>(-1, null);
  }
}
