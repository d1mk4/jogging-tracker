package io.dp.jogging.server.model;

/**
 * Created by dp on 08/09/14.
 */
public class Response<T> {

  private int status;

  private T data;

  public Response(int status, T data) {
    this.status = status;
    this.data = data;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public int getStatus() {
    return status;
  }
}
