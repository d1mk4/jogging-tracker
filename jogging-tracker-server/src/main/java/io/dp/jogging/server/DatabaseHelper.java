package io.dp.jogging.server;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.UUID;

import javax.annotation.PreDestroy;

import io.dp.jogging.server.model.OauthAccessToken;
import io.dp.jogging.server.model.OauthClientDetails;
import io.dp.jogging.server.model.OauthCode;
import io.dp.jogging.server.model.OauthRefreshToken;
import io.dp.jogging.server.model.Time;
import io.dp.jogging.server.model.User;
import io.dp.jogging.server.model.UserRole;

/**
 * Created by dp on 07/09/14.
 */
public class DatabaseHelper {
  private ConnectionSource connectionSource = null;
  private Dao<Time, UUID> timeDao = null;
  private Dao<User, String> userDao = null;
  private Dao<UserRole, Integer> userRoleDao = null;

  static {
    try {
      Class.forName("org.postgresql.Driver");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public DatabaseHelper(String url) throws SQLException {

    connectionSource = new JdbcPooledConnectionSource(url);

    try {
      TableUtils.createTableIfNotExists(connectionSource, User.class);
      TableUtils.createTableIfNotExists(connectionSource, UserRole.class);
      TableUtils.createTableIfNotExists(connectionSource, Time.class);
      TableUtils.createTableIfNotExists(connectionSource, OauthAccessToken.class);
      TableUtils.createTableIfNotExists(connectionSource, OauthClientDetails.class);
      TableUtils.createTableIfNotExists(connectionSource, OauthCode.class);
      TableUtils.createTableIfNotExists(connectionSource, OauthRefreshToken.class);
    } catch (SQLException e) {
      System.out.println("SQLException: " + e);
    }

    timeDao = DaoManager.createDao(connectionSource, Time.class);
    timeDao.setObjectCache(true);

    userDao = DaoManager.createDao(connectionSource, User.class);
    userDao.setObjectCache(true);

    userRoleDao = DaoManager.createDao(connectionSource, UserRole.class);
    userRoleDao.setObjectCache(true);
  }

  public Dao<Time, UUID> getTimeDao() {
    return timeDao;
  }

  public Dao<User, String> getUserDao() {
    return userDao;
  }

  public Dao<UserRole, Integer> getUserRoleDao() {
    return userRoleDao;
  }

  @PreDestroy
  public void destroy() {
    if (connectionSource != null) {
      connectionSource.closeQuietly();
    }
  }
}
