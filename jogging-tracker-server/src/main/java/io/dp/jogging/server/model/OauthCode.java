package io.dp.jogging.server.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dp on 13/09/14.
 */
@DatabaseTable(tableName = "oauth_code")
public class OauthCode {

  public static final String CODE = "code";

  @DatabaseField(dataType = DataType.STRING, columnName = CODE)
  private String code;

  public OauthCode() {
  }

  public String getCode() {
    return code;
  }
}
