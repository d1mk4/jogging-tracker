package io.dp.jogging.server.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dp on 13/09/14.
 */
@DatabaseTable(tableName = "user_roles")
public class UserRole {

  public static final String USER_ROLE_ID = "user_role_id";
  public static final String USERNAME = "username";
  public static final String ROLE = "role";

  @DatabaseField(generatedId = true, dataType = DataType.INTEGER, columnName = USER_ROLE_ID)
  private int userRoleId;

  @DatabaseField(uniqueCombo = true, foreign = true, foreignColumnName = User.USERNAME,
                 columnName = USERNAME)
  private User username;

  @DatabaseField(uniqueCombo = true, dataType = DataType.STRING, columnName = ROLE)
  private String role;

  public UserRole() {
  }

  public UserRole(User username, String role) {
    this.username = username;
    this.role = role;
  }

  public int getUserRoleId() {
    return userRoleId;
  }

  public User getUsername() {
    return username;
  }

  public String getRole() {
    return role;
  }
}