package io.dp.jogging.server.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dp on 13/09/14.
 */
@DatabaseTable(tableName = "oauth_refresh_token")
public class OauthRefreshToken {

  public static final String TOKEN_ID = "token_id";
  public static final String TOKEN = "token";
  public static final String AUTHENTICATION = "authentication";

  @DatabaseField(id = true, dataType = DataType.STRING, columnName = TOKEN_ID)
  private String tokenId;

  @DatabaseField(dataType = DataType.BYTE_ARRAY, columnName = TOKEN)
  private byte[] token;

  @DatabaseField(dataType = DataType.BYTE_ARRAY, columnName = AUTHENTICATION)
  private byte[] authentication;

  public OauthRefreshToken() {
  }

  public String getTokenId() {
    return tokenId;
  }

  public byte[] getToken() {
    return token;
  }

  public byte[] getAuthentication() {
    return authentication;
  }
}
