package io.dp.jogging.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dp on 13/09/14.
 */
@DatabaseTable(tableName = "users")
public class User {

  public static final String USERNAME = "username";
  public static final String PASSWORD = "password";
  public static final String ENABLED = "enabled";

  @DatabaseField(id = true, dataType = DataType.STRING, columnName = USERNAME)
  private String username;

  @JsonIgnore
  @DatabaseField(dataType = DataType.STRING, columnName = PASSWORD)
  private String password;

  @JsonIgnore
  @DatabaseField(dataType = DataType.BYTE, columnName = ENABLED)
  private byte enabled;

  public User() {
  }

  public User(String username) {
    this.username = username;
  }

  public User(String username, String password, int enabled) {
    this.username = username;
    this.password = password;
    this.enabled = (byte) enabled;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

  public byte getEnabled() {
    return enabled;
  }
}
