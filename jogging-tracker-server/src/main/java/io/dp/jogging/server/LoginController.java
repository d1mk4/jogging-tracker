package io.dp.jogging.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

import io.dp.jogging.server.model.Response;
import io.dp.jogging.server.model.User;
import io.dp.jogging.server.model.UserRole;

/**
 * Created by dp on 13/09/14.
 */

@RestController
public class LoginController {

  @Autowired
  DatabaseHelper dbHelper;

  @RequestMapping(value = "/signup", method = RequestMethod.POST)
  public Response<User> signUp(@RequestParam(value = "username") String username,
                               @RequestParam(value = "password") String password) {

    try {
      User u = dbHelper.getUserDao().createIfNotExists(new User(username, password, 1));
      if (u != null) {
        dbHelper.getUserRoleDao().createIfNotExists(new UserRole(u, "ROLE_USER"));
      }

      return new Response<User>(0, u);
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return new Response<User>(-1, null);
  }
}
