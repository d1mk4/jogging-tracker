package io.dp.jogging.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * Created by dp on 08/09/14.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class OAuth2ServerConfiguration {

  private static final String RESOURCE_ID = "restservice";

  @Configuration
  @EnableResourceServer
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  protected static class ResourceServerConfiguration extends
                                                     ResourceServerConfigurerAdapter {

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
      // @formatter:off
      resources
          .resourceId(RESOURCE_ID);
      // @formatter:on
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
      http.authorizeRequests()
          .antMatchers("/api/v1/**").authenticated();
    }
  }

  @Configuration
  @EnableAuthorizationServer
  @EnableGlobalMethodSecurity(prePostEnabled = true)
  protected static class AuthorizationServerConfiguration extends
                                                          AuthorizationServerConfigurerAdapter {

    @Autowired
    Environment env;

    @Autowired
    DataSource dataSource;

    private TokenStore tokenStore;

    @PostConstruct
    public void init() {
      tokenStore = new JdbcTokenStore(dataSource);
    }

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints)
        throws Exception {
      // @formatter:off

      endpoints
          .tokenStore(tokenStore)
          .authenticationManager(authenticationManager);
      // @formatter:on
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
      // @formatter:off

      clients
          .inMemory()
          .withClient("clientapp")
          .authorizedGrantTypes("password", "refresh_token")
          .authorities("USER")
          .scopes("read", "write")
          .resourceIds(RESOURCE_ID)
          .secret("123456")
          .accessTokenValiditySeconds(-1);
      // @formatter:on
    }

    @Bean
    public DefaultTokenServices tokenServices() {
      DefaultTokenServices tokenServices = new DefaultTokenServices();
      tokenServices.setSupportRefreshToken(true);
      tokenServices.setTokenStore(this.tokenStore);
      tokenServices.setAccessTokenValiditySeconds(0);
      return tokenServices;
    }
  }
}