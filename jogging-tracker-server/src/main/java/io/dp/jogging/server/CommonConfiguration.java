package io.dp.jogging.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.SQLException;

import javax.sql.DataSource;

/**
 * Created by dp on 07/09/14.
 */
@PropertySource("classpath:jogging.properties")
@Configuration
public class CommonConfiguration {
  @Autowired
  Environment env;

  @Bean
  public DatabaseHelper createDbHelper() throws SQLException {
    return new DatabaseHelper(env.getProperty("jdbc.url"));
  }

  @Bean
  @Scope("singleton")
  public DataSource createDataSource() throws SQLException {
    return new DriverManagerDataSource(env.getProperty("jdbc.url"));
  }
}
