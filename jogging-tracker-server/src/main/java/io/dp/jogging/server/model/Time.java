package io.dp.jogging.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;

/**
 * Created by dp on 07/09/14.
 */
@DatabaseTable(tableName = "times")
public class Time {

  public static final String TIME_ID = "time_id";
  public static final String SERVER_ID = "server_id";
  public static final String DISTANCE = "distance";
  public static final String TIME = "time";
  public static final String DATE = "date";

  @DatabaseField(generatedId = true, dataType = DataType.UUID, columnName = SERVER_ID)
  private UUID serverId;

  @DatabaseField(uniqueCombo = true, dataType = DataType.LONG, columnName = TIME_ID)
  private long timeId;

  @JsonIgnore
  @DatabaseField(foreign = true, uniqueCombo = true, foreignColumnName = User.USERNAME, columnName = User.USERNAME)
  private User user;

  @DatabaseField(dataType = DataType.INTEGER, columnName = DISTANCE)
  private int distance;

  @DatabaseField(dataType = DataType.INTEGER, columnName = TIME)
  private int time;

  @DatabaseField(dataType = DataType.LONG, columnName = DATE)
  private long date;

  public Time() {
  }

  public Time(long timeId, User user, int distance, int time, long date) {
    this.timeId = timeId;
    this.user = user;
    this.distance = distance;
    this.time = time;
    this.date = date;
  }

  public long getTimeId() {
    return timeId;
  }

  public UUID getServerId() {
    return serverId;
  }

  public int getDistance() {
    return distance;
  }

  public int getTime() {
    return time;
  }

  public long getDate() {
    return date;
  }
}
