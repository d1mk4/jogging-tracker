package io.dp.jogging.server.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dp on 13/09/14.
 */
@DatabaseTable(tableName = "oauth_access_token")
public class OauthAccessToken {

  public static final String TOKEN_ID = "token_id";
  public static final String TOKEN = "token";
  public static final String AUTHENTICATION_ID = "authentication_id";
  public static final String USER_NAME = "user_name";
  public static final String CLIENT_ID = "client_id";
  public static final String AUTHENTICATION = "authentication";
  public static final String REFRESH_TOKEN = "refresh_token";
  public static final String ACCESS_TOKEN_VALIDITY = "access_token_validity";
  public static final String REFRESH_TOKEN_VALIDITY = "refresh_token_validity";

  @DatabaseField(id = true, dataType = DataType.STRING, columnName = TOKEN_ID)
  private String tokenId;

  @DatabaseField(dataType = DataType.BYTE_ARRAY, columnName = TOKEN)
  private byte[] token;

  @DatabaseField(dataType = DataType.STRING, columnName = AUTHENTICATION_ID)
  private String authenticationId;

  @DatabaseField(dataType = DataType.STRING, columnName = USER_NAME)
  private String userName;

  @DatabaseField(dataType = DataType.STRING, columnName = CLIENT_ID)
  private String clientId;

  @DatabaseField(dataType = DataType.BYTE_ARRAY, columnName = AUTHENTICATION)
  private byte[] authentication;

  @DatabaseField(dataType = DataType.STRING, columnName = REFRESH_TOKEN)
  private String refreshToken;

  @DatabaseField(dataType = DataType.INTEGER, columnName = ACCESS_TOKEN_VALIDITY)
  private int accessTokenValidity;

  @DatabaseField(dataType = DataType.INTEGER, columnName = REFRESH_TOKEN_VALIDITY)
  private int refreshTokenVAlidity;

  public OauthAccessToken() {
  }

  public String getTokenId() {
    return tokenId;
  }

  public byte[] getToken() {
    return token;
  }

  public String getAuthenticationId() {
    return authenticationId;
  }

  public String getUserName() {
    return userName;
  }

  public String getClientId() {
    return clientId;
  }

  public byte[] getAuthentication() {
    return authentication;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public int getAccessTokenVAlidity() {
    return accessTokenValidity;
  }

  public int getRefreshTokenVAlidity() {
    return refreshTokenVAlidity;
  }
}
