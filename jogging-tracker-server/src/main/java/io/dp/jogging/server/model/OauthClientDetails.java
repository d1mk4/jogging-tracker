package io.dp.jogging.server.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;

/**
 * Created by dp on 13/09/14.
 */
@DatabaseTable(tableName = "oauth_client_details")
public class OauthClientDetails {

  public static final String CLIENT_ID = "client_id";
  public static final String RESOURCE_IDS = "resource_ids";
  public static final String CLIENT_SECRET = "client_secret";
  public static final String SCOPE = "scope";
  public static final String AUTHORIZED_GRANT_TYPES = "authorized_grant_types";
  public static final String WEB_SERVER_REDIRECT_URI = "web_server_redirect_uri";
  public static final String AUTHORITIES = "authorities";
  public static final String ACCESS_TOKEN_VALIDITY = "access_token_validity";
  public static final String REFRESH_TOKEN_VALIDITY = "refresh_token_validity";
  public static final String AUTOAPPROVE = "autoapprove";
  public static final String ADDITIONAL_INFORMATION = "additional_information";

  @DatabaseField(id = true, dataType = DataType.STRING, columnName = CLIENT_ID)
  private String clientId;

  @DatabaseField(dataType = DataType.STRING, columnName = RESOURCE_IDS)
  private String resourceIds;

  @DatabaseField(dataType = DataType.STRING, columnName = CLIENT_SECRET)
  private String clientSecret;

  @DatabaseField(dataType = DataType.STRING, columnName = SCOPE)
  private String scope;

  @DatabaseField(dataType = DataType.STRING, columnName = AUTHORIZED_GRANT_TYPES)
  private String authorizedGrantTypes;

  @DatabaseField(dataType = DataType.STRING, columnName = WEB_SERVER_REDIRECT_URI)
  private String webServerRedirectUri;

  @DatabaseField(dataType = DataType.STRING, columnName = AUTHORITIES)
  private String authorities;

  @DatabaseField(dataType = DataType.INTEGER, columnName = ACCESS_TOKEN_VALIDITY)
  private int accessTokenValidity;

  @DatabaseField(dataType = DataType.INTEGER, columnName = REFRESH_TOKEN_VALIDITY)
  private int refreshTokenValidity;

  @DatabaseField(dataType = DataType.STRING, columnName = AUTOAPPROVE)
  private String autoapprove;

  @DatabaseField(dataType = DataType.STRING, columnName = ADDITIONAL_INFORMATION)
  private String additionalInformation;

  public OauthClientDetails() {
  }

  public String getClientId() {
    return clientId;
  }

  public String getResourceIds() {
    return resourceIds;
  }

  public String getClientSecret() {
    return clientSecret;
  }

  public String getScope() {
    return scope;
  }

  public String getAuthorizedGrantTypes() {
    return authorizedGrantTypes;
  }

  public String getWebServerRedirectUri() {
    return webServerRedirectUri;
  }

  public String getAuthorities() {
    return authorities;
  }

  public int getAccessTokenValidity() {
    return accessTokenValidity;
  }

  public int getRefreshTokenValidity() {
    return refreshTokenValidity;
  }

  public String getAutoapprove() {
    return autoapprove;
  }

  public String getAdditionalInformation() {
    return additionalInformation;
  }
}
